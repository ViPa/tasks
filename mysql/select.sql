/* ------------------- 2. PRODUCT - ATTRIBUTES -------------------*/

/* Select products which have attributes with all their attributes */
SELECT p.name, a.attr_name, a.attr_value 
    FROM products p
INNER JOIN attributes a on p.id = a.product_id; 

/* Select only products name which have attributes */
SELECT DISTINCT p.name 
    FROM products p
INNER JOIN attributes a on p.id = a.product_id; 

/* Select products which havn't attributes */
SELECT p.name as product_name, a.attr_name, a.attr_value 
    FROM products p
LEFT JOIN attributes a on p.id = a.product_id
    WHERE a.attr_name IS NULL;


/* ------------------- 3. PRODUCT - CATEGORIES -------------------*/
/* select products with categories */
SELECT c.name as category, p.name, p.price, p.amount
    FROM relations r
INNER JOIN products p on p.id = r.product_id
INNER JOIN categories c on c.id = r.category_id
ORDER BY category;

/* same as previous but using another tables order */
SELECT c.name as category, p.name, p.price, p.amount
    FROM products p
INNER JOIN relations r on p.id = r.product_id
INNER JOIN categories c on c.id = r.category_id
ORDER BY category;

/* ------------------- 4. PRODUCT - CATEGORIES -------------------*/
/* select products with categories and their attributes */
SELECT c.name as category, p.name, a.attr_name, a.attr_value
    FROM relations r
INNER JOIN products p on p.id = r.product_id
INNER JOIN categories c on c.id = r.category_id
INNER JOIN attributes a on a.product_id = p.id
    WHERE c.id = 10;

/* select products with categories and their attributes including products without attributes */
SELECT c.name as category, p.name, a.attr_name, a.attr_value
    FROM relations r
INNER JOIN products p on p.id = r.product_id
INNER JOIN categories c on c.id = r.category_id
LEFT JOIN attributes a on a.product_id = p.id
    WHERE c.id = 10;