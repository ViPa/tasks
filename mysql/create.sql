/* create new database and use it */
DROP DATABASE IF EXISTS store;
CREATE DATABASE store;

USE store;

/* tables creating */
CREATE TABLE IF NOT EXISTS products (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    price float,
    amount int
);

CREATE TABLE IF NOT EXISTS categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS relations (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT NOT NULL,
    category_id INT NOT NULL,
    FOREIGN KEY (product_id) REFERENCES products (id),
    FOREIGN KEY (category_id) REFERENCES categories (id)
);

CREATE TABLE IF NOT EXISTS attributes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT NOT NULL,
    attr_name VARCHAR(20) NOT NULL,
    attr_value VARCHAR(50) NOT NULL,
    FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE
);

/* inserting values */
INSERT INTO products (name, price, amount) VALUES
    ('Laptop ASUS X515EA', 530.99, 25),
    ('Laptop ACER Aspire A315', 400.99, 35),
    ('Laptop HP 15s', 599.95, 50),
    ('Laptop HP Pavilion Gaming 15e', 1129.95, 10),
    ('Laptop Apple MacBook Air 13', 1289.99, 5),
    ('Samsung Galaxy M52-5G', 439.95, 20),
    ('Xiaomi 11T', 549.25, 15),
    ('Xiaomi Redme Note 9 Pro', 285.99, 40),
    ('Comanche bike Tomahawk Disc 26', 380.00, 10),
    ('Comanche bike ONTARIO FLY 26', 265.99, 10),
    ('Pride bike BULLET 28 2020', 676.95, 5),
    ('Pride bike MINI 1', 249.99, 25);

INSERT INTO attributes (product_id, attr_name, attr_value) VALUES
    (1, 'CPU', 'Core i5'),
    (1, 'RAM', 'Kingston DDR4 8Gb'),
    (1, 'HDD', 'Hitachi 1Tb 7200/8'),
    (2, 'CPU', 'Core i7'),
    (2, 'RAM', 'GoodRAM DDR4 16Gb'),
    (2, 'HDD', 'Seagate 2Tb 5400/8'),
    (3, 'CPU', 'Core i9'),
    (3, 'RAM', 'Samsung DDR4L 8Gb'),
    (3, 'HDD', 'WD 500Gb 7200/8'),
    (4, 'CPU', 'AMD Turion XX'),
    (4, 'RAM', 'Kingston DDR4L 32Gb'),
    (4, 'SSD', 'Transcend 256Gb'),
    (6, 'Screen', 'Super AMOLED Plus 6.7" 2400x1080'),
    (6, 'Features', '6/128Gb Light Blue'),
    (7, 'Screen', 'AMOLED 6.67"'),
    (7, 'Features', '8/128Gb White'),
    (8, 'Screen', 'IPS 6.67"'),
    (8, 'Features', '6/128Gb Tropical Green');

INSERT INTO categories (name) VALUES
    ('Laptops and Desktops'),
    ('Smartphones and TV'),
    ('Gamers products'),
    ('Household appliances'),
    ('Tools'),
    ('Car goods'),
    ('Sports and hobbies'),
    ('Health and beauty'),
    ('Baby goods'),
    ('Discounts and actions');

INSERT INTO relations (product_id, category_id) VALUES
    (1, 1), (1, 10),
    (2, 1),
    (3, 1),
    (4, 1),
    (5, 1),
    (6, 2),
    (7, 2),
    (8, 2), (8, 10),
    (9, 7),
    (10, 7),
    (11, 7),
    (12, 7), (12, 10);