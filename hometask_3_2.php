<?php

//---------------------------------------------------
/**
 * Print "Fizz", "Buzz", "FizzBuzz" or integer value
 * from 1 to 100 depending of the situation with multiples
 * of 3, 5 and both of them 
 */
function printFizzBuzz() {
    for($i = 1; $i <= 100; $i++) {
        if ($i % 15 == 0) {
            echo "FizzBuzz" . "\n";
        } elseif ($i % 3 == 0) {
            echo "Fizz" . "\n";
        } elseif ($i % 5 == 0) {
            echo "Buzz" . "\n";
        } else {
            echo $i . "\n";
        }
    }
}

printFizzBuzz();

?>