<?php

//---------------------------------------------------
/**
 * Check if last 2 chars is mirrored to first 2 chars
 *
 * @param string $word
 * @return bool
 */
function hasMirroredLastChars($word) {
    if (strlen($word) >= 2) {
        $mirrorWord = strrev($word);
        if ($word[0] == $mirrorWord[0] && $word[1] == $mirrorWord[1]) {
            return true;
        }
    } else {
        return false;
    }
}

$dictionary = ["abba", "obana", "aha-ha", "qwertyqw", "qwewqeqwewrewq", "ababagalamaga", "ababagalamaba", "a", "the", "windows", "linux-xunil"];

foreach($dictionary as $word) {
    echo hasMirroredLastChars($word) ? $word . "\n" : "";
}

?>