<?php 

    class Hello {
        public function showIntro(string $name = "Vetal") {
            echo "Hello All, I am " . $name . "\n";
        }
    }

    $object = new Hello();
    $object->showIntro();
    $object->showIntro("Elogic");

?>