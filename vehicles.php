<?php
 require_once "vehicle.php";

class Supercar extends Car {
    protected $maker;
    protected $model;
    protected $price;
    protected $color;
     
    public function __construct(int $speed, int $countOfPlaces, string $maker, string $model, float $price, string $color) {
        parent::__construct($speed, $countOfPlaces);
        $this->maker = $maker;
        $this->model = $model;
        $this->price = $price;
        $this->color = $color;
    }

    public function getMaker() {
        return $this->maker;
    }

    public function getModel() {
        return $this->model;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getColor() {
        return $this->color;
    }

    public function getInfo() {
        return parent::getInfo() . 
            "Manufactured by - " . $this->getMaker() . "-\n" . 
            "Model: " . $this->getModel() . ". Price, USD: " . $this->getPrice() . ". Color: " . $this->getColor() . "\n";
    }

 }

class Superbike extends Bike {
    protected $maker;
    protected $engine;
    protected $price;
    protected $color;
     
    public function __construct(int $speed, int $countOfPlaces, string $maker, string $engine, float $price, string $color) {
        parent::__construct($speed, $countOfPlaces);
        $this->maker = $maker;
        $this->engine = $engine;
        $this->price = $price;
        $this->color = $color;
    }

    public function getMaker() {
        return $this->maker;
    }

    public function getEngine() {
        return $this->engine;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getColor() {
        return $this->color;
    }

    public function getInfo() {
        return parent::getInfo() . 
            "Manufactured by - " . $this->getMaker() . "-\n" . 
            "Engine: " . $this->getEngine() . ". Price, USD: " . $this->getPrice() . ". Color: " . $this->getColor() . "\n";
    }

 }

class Store {
    private $storeName;
    protected $cars;
    protected $bikes;

    public function __construct($name) {
        $this->storeName = $name;
        $this->cars = [];
        $this->bikes = [];
    }

    public function addCar(Supercar $car) {
        $this->cars[] = $car;
    }

    public function addBike(Superbike $bike) {
        $this->bikes[] = $bike;
    }

    public function showCatalog() {
        $catalog = "##### " . $this->storeName . " store catalog #####\n";
        if (count($this->cars) > 0) {
            $catalog .= "----------- CARS -------------\n";
            foreach($this->cars as $item) {
                $catalog .= $item->getInfo();
            }

        }
        if (count($this->bikes) > 0) {
            $catalog .= "----------- BIKES -------------\n";
            foreach($this->bikes as $item) {
                $catalog .= $item->getInfo();
            }
        }
        return $catalog;
    }

}

$store = new Store("The coolest vehicles");
$store->addBike(new Superbike(290, 2, "Yamaha", "V350", 17000, "magenta")); 
$store->addCar(new Supercar(250, 2, "VolksWagen", "Golf V", 20000, "oliva"));

echo $store->showCatalog();

?>