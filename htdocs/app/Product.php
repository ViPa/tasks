<?php
/*
 * Copyright (c) 2021.
 */
namespace App;

use App\Interfaces\ProductInterface;

/**
 * class Product
 */
class Product implements ProductInterface
{
    protected $name;
    protected $price;
    protected $rules = [];

    /**
     * Set product name
     *
     * @param string $name
     * @return void
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get product name
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    
    /**
     * Set product price
     *
     * @param float $price
     * @return void
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * Get product price
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Set pricing rule for product 
     *
     * @param integer $count
     * @param float $price
     * @return void
     */
    public function setRule(int $count, float $price)
    {
        $this->rules[] = [$count, $price];        
    }

    /**
     * Get pricing rule for product
     *
     * @return array
     */
    public function getRule(): array
    {
        return $this->rules;   
    }

}
