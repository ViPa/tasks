<?php
/*
 * Copyright (c) 2021.
 */

namespace App;

use App\Interfaces\CalculatorInterface;

/**
 * class Calculator
 */
class Calculator implements CalculatorInterface
{
    /**
     * Calculate total sum for all products in quote using prices per unit and volume prices
     *
     * @param Store $store
     * @param Quote $quote
     * @return float
     */
    public function total(Store $store, Quote $quote): float
    {
        $summa = 0;
        foreach($quote->getList() as $item=>$count){
            $product = $store->getProductByName($item);
            if ($product->getRule() !== []) {
                $rulePortions = floor($count / $product->getRule()[0][0]);
                $summa += $rulePortions * $product->getRule()[0][1];
                $restPortion = $count % $product->getRule()[0][0];
                $summa += $restPortion * $product->getPrice();
            } else {
                $summa += $count * $product->getPrice();
            }
        }

        return $summa;
    }
}