<?php
/*
 * Copyright (c) 2021.
 */

namespace App\Interfaces;

/**
 * interface QuoteInterface.
 */
interface QuoteInterface
{
    /**
     * Add product to quote.
     *
     * @param \App\Product $product
     * @return void
     */
    public function scan(\App\Product $product);

    /**
     * Add array of products to quote
     *
     * @param array $productLine
     * @return void
     */
    public function scanLine(array $productLine);

    /**
     * Clear the quote
     *
     * @return void
     */
    public function clearQuote();

    /**
     * Get list of all products.
     *
     * @return array
     */
    public function getList(): array;
}

?>