<?php
/*
 * Copyright (c) 2021.
 */

namespace App\Interfaces;

/**
 * interface CalculatorInterface.
 */
interface CalculatorInterface
{
    /**
     * * Calculate total sum for all products in quote using prices per unit and volume prices
     *
     * @param \App\Store $store
     * @param \App\Quote $quote
     * @return float
     */
    public function total(\App\Store $store, \App\Quote $quote): float;

}

?>