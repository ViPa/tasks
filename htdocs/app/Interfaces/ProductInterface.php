<?php
/*
 * Copyright (c) 2021.
 */

namespace App\Interfaces;

/**
 * interface ProductInterface.
 */
interface ProductInterface
{
    /**
     * Set name of product.
     *
     * @param string $name
     */
    public function setName(string $name);

    /**
     * Set price of product
     *
     * @param float $price
     * @return void
     */
    public function setPrice(float $price);

    /**
     * Get name of product.
     * 
     * @return string
     */
    public function getName() : string;
    
    /**
     * Get price of product 
     *
     * @return float
     */
    public function getPrice() : float;

    /**
     * Set pricing rule for product
     *
     * @param integer $count
     * @param float $price
     * @return void
     */
    public function setRule(int $count, float $price);

    /**
     * Get pricing rule for product
     *
     * @return array
     */
    public function getRule(): array;
}