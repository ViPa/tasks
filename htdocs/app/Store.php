<?php
/*
 * Copyright (c) 2021.
 */
namespace App;

use App\Interfaces\ProductInterface;

/**
 * Class Store
 */
class Store
{
    /**
     * Array of products in store
     *
     * @var array
     */
    protected $products = [];

    /**
     * Add product to the store
     *
     * @param Product $product
     * @return void
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }

    /**
     * Looking for the product in the store and return it's index or -1
     *
     * @param string $name
     * @return integer
     */
    public function hasProduct(string $name): int 
    {
        foreach($this->products as $key=>$item) {
            if ($item->getName() == $name) {
                return $key;
            }
        }

        return -1;
    }

    /**
     * Return product object by it's name
     *
     * @param string $name
     * @return Product
     */
    public function getProductByName(string $name): Product
    {
        $key = $this->hasProduct($name);
        if ($key  !== -1) {
            return $this->products[$key];
        }
    }

}
