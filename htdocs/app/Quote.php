<?php
/*
 * Copyright (c) 2021.
 */

namespace App;

use App\Interfaces\QuoteInterface;

/**
 * class Quote
 */
class Quote implements QuoteInterface
{
    /**
     * List products.
     *
     * @var array
     */
    protected $listOfProducts = [];

    /**
     * Add product to quote.
     *
     * @param Product $product
     */
    public function scan(Product $product)
    {
        if (isset($this->listOfProducts[$product->getName()])) {
            $this->listOfProducts[$product->getName()]++;
        } else {
            $this->listOfProducts[$product->getName()] = 1;
        }
    }

    /**
     * Add array of products to quote
     *
     * @param array $productLine
     * @return void
     */
    public function scanLine(array $productLine) 
    {
        foreach($productLine as $item) {
            $this->scan($item);
        }
    }

    /**
     * Clear the quote
     *
     * @return void
     */
    public function clearQuote() 
    {
        $this->listOfProducts = [];
    }

    /**
     * Get list of all products.
     *
     * @return array
     */
    public function getList(): array
    {
        return $this->listOfProducts;
    }

}