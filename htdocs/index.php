<?php
// try catch, static, interface

use App\Calculator;
use App\Product;
use App\Quote;
use App\Store;

require_once('vendor/autoload.php');

$a = new Product();
$a->setName('A');
$a->setPrice(2.00);
$a->setRule(4, 7.00);

$b = new Product();
$b->setName('B');
$b->setPrice(12.00);

$c = new Product();
$c->setName('C');
$c->setPrice(1.25);
$c->setRule(6, 6.00);

$d = new Product();
$d->setName('D');
$d->setPrice(0.15);

$store = new Store();
$store->addProduct($a);
$store->addProduct($b);
$store->addProduct($c);
$store->addProduct($d);

$calculator = new Calculator();
$terminal = new Quote();

$terminal->scanLine([$a, $b, $c, $d, $a, $b, $a, $a]);
print_r($terminal->getList());
echo "Total: $" . $calculator->total($store, $terminal) . "\n";     

$terminal->clearQuote();
$terminal->scanLine([$c, $c, $c, $c, $c, $c, $c]);
print_r($terminal->getList());
echo "Total: $" . $calculator->total($store, $terminal) . "\n";     

$terminal->clearQuote();
$terminal->scanLine([$a, $b, $c, $d]);
print_r($terminal->getList());
echo "Total: $" . $calculator->total($store, $terminal) . "\n";     
