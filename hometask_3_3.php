<?php

//-----------------------------------------------------
/**
 * Show formatted user information from the given array. 
 *
 * @param array $data
 * 
 */
function showUserInfo($data) {
    $infoString = "";
    foreach($data as $userItem) {
        
        if ($userItem["children"]) {
            $childrenNames = implode(", ", $userItem["children"]);    
            $childrenInfo = "має " . count($userItem["children"]) . " дітей та їх звати " . $childrenNames . ",\n\t";
        } else {
            $childrenInfo = "Дітей не має" . "\n\t";
        }

        $infoString .= "Дані про користувача " . $userItem["name"] . ":" . "\n\t" .
                        "народився(-лася) " . (intval(date("Y")) - $userItem["age"]) . ",\n\t" .
                        $childrenInfo . 
                        ($userItem["children"] ? "їх бабусю" : "маму") .
                        " звати " . $userItem["mother"]["name"] . ", яка народилася " . (intval(date("Y")) - $userItem["mother"]["age"]) . ".";
        $infoString .= $userItem["married"] ? "\n\t" . "Одружений." : "\n\t" . "Неодружений.";    
        $infoString .= $userItem["dog"] ?  " Має собаку." : "";             
        echo $infoString . "\n-----------------------\n";
        $infoString = "";
    }
}

$json = '{
    "ivan":{
        "name": "Іван",
        "age": 37,
        "mother": {
            "name": "Марія",
            "age": 58
        },
        "children": [
            "Маша",
            "Ігор",
            "Таня"
        ],
        "married": true,
        "dog": true
    },
    "serhii":{
        "name": "Сергій",
        "age": 37,
        "mother": {
            "name": "Ольга",
            "age": 76
        },
        "children": [
            "Маша"
        ],
        "married": true,
        "dog": null
    },
    "nata":{
        "name": "Наталя",
        "age": 37,
        "mother": {
            "name": "Ольга",
            "age": 89
        },
        "children": [
            "Ігор",
            "Таня"
        ],
        "married": false,
        "dog": null
    },
    "vova":{
        "name": "Володя",
        "age": 7,
        "mother": {
            "name": "Ольга",
            "age": 29
        },
        "married": false,
        "dog": true
    }
}';

$data = json_decode($json, true);

showUserInfo($data);

?>