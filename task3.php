<?php

function randomTempArray($from, $to, $count) {
    for ($i = 0; $i < $count; $i++) {
        $tempArray[] = rand($from, $to);
    }
    return $tempArray;
}

function CalcAverageTemperature($tempArray) {
    if (!is_array($tempArray)) {
        return "Please input temperature array values";
    }
    
    $sum = 0;
    foreach($tempArray as $values) {
        $sum += $values;
    }
    return $sum / count($tempArray);
}

$temperatureValues = [78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73];
$temperatureArray = $temperatureValues ?? randomTempArray(60, 90, 30); 
echo "Average Temperature is: ", CalcAverageTemperature($temperatureArray), "\n";
sort($temperatureArray);
$uniqueValues = array_unique($temperatureArray);
//print_r($uniqueValues);
$lowest = implode(", ", array_slice($uniqueValues, 0, 5));
$highest = implode(", ", array_slice($uniqueValues, -5));
echo "List of five lowest temperatures : ", $lowest, "\n";
echo "List of five highest temperatures : ", $highest, "\n";
?>