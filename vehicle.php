<?php 

abstract class Vehicle {
    protected $speed;
    protected $countOfPlaces;

    public function __construct(int $speed, int $countOfPlaces) {
        $this->speed = $speed;
        $this->countOfPlaces = $countOfPlaces;
    }

    public function getSpeed() {
        return $this->speed;
    }

    public function getCountOfPlaces() {
        return $this->countOfPlaces;
    }

    abstract public function getInfo();
}

class Car extends Vehicle {
    protected $wheels;

    public function __construct(int $speed, int $countOfPlaces, int $wheels = 4) {
        parent::__construct($speed, $countOfPlaces);
        $this->wheels = $wheels;
    }

    public function getNumberOfWheels() {
        return $this->wheels;
    }

    public function getInfo() {
        return "Type of vehicle: {" . __CLASS__ . "} ---> " . "\n" . 
                "Speed: " . $this->getSpeed() . ", " . 
                "Count of places: " . $this->getCountOfPlaces() . ", " . 
                "Number of wheels: " . $this->getNumberOfWheels() . "\n";
    }
}

class Bike extends Vehicle {
    protected $wheels;

    public function __construct(int $speed, int $countOfPlaces, int $wheels = 2) {
        parent::__construct($speed, $countOfPlaces);
        $this->wheels = $wheels;
    }

    public function getNumberOfWheels() {
        return $this->wheels;
    }

    public function getInfo() {
        return "Type of vehicle: {" . __CLASS__ . "} ---> " . "\n" . 
                "Speed: " . $this->getSpeed() . ", " . 
                "Count of places: " . $this->getCountOfPlaces() . ", " . 
                "Number of wheels: " . $this->getNumberOfWheels() . "\n";
    }
}

class Airplane extends Vehicle {
    protected $wheels;
    protected $loadCapacity;

    public function __construct(int $speed, int $countOfPlaces, int $wheels, int $loadCapacity) {
        parent::__construct($speed, $countOfPlaces);
        $this->wheels = $wheels;
        $this->loadCapacity = $loadCapacity;
    }

    public function getNumberOfWheels() {
        return $this->wheels;
    }

    public function getLoadCapacity() {
        return $this->loadCapacity;
    }

    public function getInfo() {
        return "Type of vehicle: {" . __CLASS__ . "} ---> " . "\n" . 
                "Speed: " . $this->getSpeed() . ", " . 
                "Count of places: " . $this->getCountOfPlaces() . ", " . 
                "Number of wheels: " . $this->getNumberOfWheels() . ", " .
                "Load Capacity: " . $this->getLoadCapacity() . "\n";
    }
}
/** 
$car = new Car(160, 5);
echo $car->getInfo();

$moto = new Bike(220, 2);
echo $moto->getInfo();

$plane = new Airplane(900, 80, 3, 100);
echo $plane->getInfo();
**/
?>